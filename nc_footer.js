      function createFooter() {
        
        const copyright = document.createElement('p')
        copyright.innerHTML = FOOTER_COPYRIGHT
        copyright.style.color = '#6E7883'
        copyright.style.margin = '20px 0 0 0'
    
        const img = document.createElement("img")
        img.setAttribute('src', '')
        img.style.width = '33px'
        img.setAttribute('class', 'logo')
    
        const footerLinks = document.createElement('div')
        footerLinks.setAttribute('class', 'footer-links')

        Object.keys(FOOTER_LINKS).forEach(name => {
          let link = document.createElement("a")
          link.innerHTML = name
          link.setAttribute('href', FOOTER_LINKS[name])
          link.setAttribute('class', 'link')
          footerLinks.appendChild(link)
        });
  
        const footer = document.createElement('div')
        footer.setAttribute('class', 'footer')
        /*footer.appendChild(img)*/
        footer.appendChild(footerLinks)
        footer.appendChild(copyright)

        return footer;
      }

      window.onload = (event) => {
    
        setInterval(
        () => {  
          let footerNode = document.getElementsByClassName("footer")[0];
          let pageContentNode = document.getElementsByClassName("notion-page-content")[0];

          let footerExists = typeof(footerNode) != 'undefined' && footerNode != null;
          let pageExists = typeof(pageContentNode) != 'undefined' && pageContentNode != null;

          if (!footerExists && pageExists) {
            let lfooter = createFooter();
            let notionApp = document.getElementsByClassName("notion-scroller")[0];
            notionApp.appendChild( lfooter ); 
          }
          
        }, 1500);

      }
