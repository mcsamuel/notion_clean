    document.addEventListener("readystatechange", (event) => {

    if (document.readyState !== "complete") return false;
    const attempt_interval = setInterval(enhance, 500);

    function enhance() {
      const notion_elem = document.querySelector(".notion-app-inner");
      if (!notion_elem) return;
      clearInterval(attempt_interval);
      const observer = new MutationObserver(handle);
      observer.observe(notion_elem, {
        childList: true,
        subtree: true,
      });

      handle();
      function handle(list, observer) {

          /*Bypass preview*/
          const preview = document.querySelector(
            '.notion-peek-renderer [style*="height: 45px;"] a'
          );
          if (preview) {
            preview.click();
          }

      }
    }
    });
