/* Check if we need to override Notion's fav icon */
if (FAV_ICON !== '') {
  setInterval(
      () => {  
        /*Add a fav icon*/
        var link = document.querySelector("link[rel~='icon']");
        if (!link) {
            link = document.createElement('link');
            link.rel = 'icon';
            document.getElementsByTagName('head')[0].appendChild(link);
        }
        link.href = FAV_ICON;

      }, 500);
}
